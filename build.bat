call "%VS120COMNTOOLS%\..\..\VC\vcvarsall.bat" x64

set SOURCE_DIR=%~dp0

if exist CMakeLists.txt (
	echo We seem to be in the source tree
	if exist build (
		echo Using existing build dir.
	) else (
		echo Creating new build dir.
		mkdir build
	)
	cd build
) else (
	echo The file does not exist.
)
echo SOURCE_DIR is %SOURCE_DIR%
echo BUILD_DIR is %CD%

echo Invoking CMake.
cmake %SOURCE_DIR% -G"Visual Studio 12 2013 Win64"

echo Starting build
msbuild /p:Configuration=Release /p:Platform=x64 ALL_BUILD.vcxproj
pause